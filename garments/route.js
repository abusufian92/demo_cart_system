var app = angular.module('hello', ['ngRoute']);

app.config(function($routeProvider) {
  $routeProvider

  .when('/', {
    templateUrl : 'index.html',
    controller  : 'helloCtrl'
  })

  .when('/male', {
    templateUrl : 'male.html',
  })

  .when('/female', {
    templateUrl : 'female.html',
  })
 
  .otherwise({redirectTo: '/'});
  
});

app.controller('helloCtrl', function($scope) {

  $scope.cart = [];
      $scope.addToCart = function (product) {
      var found = false;
      $scope.cart.forEach(function (item) {
        if (item.id === product.id) {
          item.quantity++;
          found = true;
        }
      });
      if (!found) {
        $scope.cart.push(angular.extend({quantity: 1}, product));
      }
    };

    $scope.deleteToCart = function (product,x) {

      $scope.cart.forEach(function (item) {

        if (item.id === product.id) {
      item.quantity=item.quantity-1;
      if (item.quantity==0) {
        $scope.cart.splice(x, 1);
      }
          
        }

      });

    };

    $scope.display = function (x) {

               $scope.displayimage = x;
    };


    $scope.removeToCart = function (x) {
               $scope.cart.splice(x, 1);
    };


    $scope.getCartPrice = function () {
      var total = 0;
      $scope.cart.forEach(function (product) {
        total += product.price * product.quantity;
      });
      return total;
    };

    $scope.calculate = function (x,y) {
      var amount = x*y;
      return amount;
    };

     $scope.checkout = function () {
         //alert('done');
           //location.reload();
           var output="";
           $scope.cart.forEach(function (item) {
            output= output+" "+item.quantity+" piece "+item.title+"\n";
      });

           var total = 0;
      $scope.cart.forEach(function (product) {
        total += product.price * product.quantity;
      });

            var name = $scope.name;
            var email = $scope.email;
            var phn = $scope.phn;

            //alert(name+"\n"+email+"\n"+phn);
           //alert(output);
           var res = encodeURI(output);
           //alert(res);
  window.location.href="mail.php?order="+res+"&price="+total+"&name="+name+"&email="+email+"&phn="+phn;
   };

  
});

